<?php

namespace backend\modules\documentation\models;

use Yii;

/**
 * This is the model class for table "yiidoc_sections".
 *
 * @property integer $id
 * @property integer $documentation_id
 * @property string $name
 *
 * @property Documentations $documentation
 *
 */
class Sections extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sections}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['documentation_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['documentation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Documentations::className(), 'targetAttribute' => ['documentation_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'documentation_id' => 'Documentation ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentation()
    {
        return $this->hasOne(Documentations::className(), ['id' => 'documentation_id']);
    }
}
