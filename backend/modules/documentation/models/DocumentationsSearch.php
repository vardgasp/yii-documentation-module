<?php

namespace backend\modules\documentation\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\documentation\models\Documentations;

/**
 * DocumentationsSearch represents the model behind the search form about `backend\modules\documentation\models\Documentations`.
 */
class DocumentationsSearch extends Documentations
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['title', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Documentations::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        if ($this->created_at) {
            $start = strtotime($this->created_at);
            $end = strtotime($this->created_at . ' 23:59:59');
            $this->created_at = $start;
            $query->andFilterWhere(['between', 'created_at', $start, $end]);
        }

        if ($this->updated_at) {
            $start = strtotime($this->updated_at);
            $end = strtotime($this->updated_at . ' 23:59:59');
            $this->updated_at = $start;
            $query->andFilterWhere(['between', 'updated_at', $start, $end]);
        }

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
