<?php

namespace backend\modules\documentation\assets;

use \yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class DocumentationAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/documentation';
    public $css = [
        'css/sections.css',
        'css/widget.css',
    ];

    public $js = [
        'http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js',
        'ckeditor/ckeditor.js',
        'js/sections.js',
        'js/widget.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];


}

/*

        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
 */