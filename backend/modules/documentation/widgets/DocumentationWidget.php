<?php

namespace backend\modules\documentation\widgets;

use backend\modules\documentation\models\Documentations;
use backend\modules\documentation\models\Sections;
use Yii;
use yii\base\Widget;

/**
 * DocumentationWidget .
 *
 */
class DocumentationWidget extends Widget
{
    public $id;
    public $showMenu = true;

    public function run()
    {
        $showMenu = $this->showMenu;
        $sections = Sections::find()->where(['documentation_id' => $this->id])->orderBy('sort_id ASC')->all();
        return $this->render('DocumentationWidget', compact('sections', 'showMenu'));
    }
}
