<?php

/* @var $sections \backend\modules\documentation\models\Sections */
use backend\modules\documentation\assets\DocumentationAsset;

DocumentationAsset::register($this);
?>
<div class="container-fluid posRel">
    <div class="row">
        <?php if($showMenu) : ?>
        <div class="col-md-12 documentationTitle"
             style="color: <?= $sections[0]->documentation->header_text_color != '' ? $sections[0]->documentation->header_text_color : '#29413c' ?>;background: <?= $sections[0]->documentation->header_bg_color != '' ? $sections[0]->documentation->header_bg_color : '#e0e0e0' ?>;">
            Documentation : <?= $sections[0]->documentation->title ?></div>
        <?php else : ?>
            <div class="col-md-12 documentationTitle" style="font-weight:bold;">
                Documentation : <?= $sections[0]->documentation->title ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="row documentationInfo">
        <div class="col-md-2">Created At
            : <?= Yii::$app->formatter->asDate($sections[0]->documentation->created_at, 'dd-MM-yyyy'); ?></div>
        <div class="col-md-2">Updated At
            : <?= Yii::$app->formatter->asDate($sections[0]->documentation->updated_at, 'dd-MM-yyyy'); ?></div>
        <div class="col-md-2">Version : <?= $sections[0]->documentation->version  ? $sections[0]->documentation->version : 'No Version'?></div>
        <div class="col-md-3">Support : <a
                href="<?= $sections[0]->documentation->support_link ? 'http://'.$sections[0]->documentation->support_link : '#' ?>"><?= $sections[0]->documentation->support_text ? $sections[0]->documentation->support_text : 'No Support' ?></a>
        </div>
        <div class="col-md-3">Author : <a
                href="<?= $sections[0]->documentation->author_profile ? 'http://'.$sections[0]->documentation->author_profile : '#' ?>"><?= $sections[0]->documentation->author_name ? $sections[0]->documentation->author_name : 'No Author' ?></a>
        </div>
    </div>
    <div class="row">
        <?php if($showMenu) : ?>
            <style>
                .documentationInfo > div {
                    font-weight: bold !important;
                }
            </style>
        <div class="col-md-3 fixedLeft" style="padding-left:0;">
            <ul class="nav nav-pills nav-stacked">
                <?php
                foreach ($sections as $section) {
                    echo '<li><a href="#" class="scroll-link" data-id="' . $section->sort_id . '">' . $section->name . '</a></li>';
                }
                ?>
            </ul>
        </div>
        <?php endif; ?>
        <div class="col-md-9" style="padding-right:0;">
            <?php
            foreach ($sections as $section) {
                echo '<div id="' . $section->sort_id . '"  class="page-section">
                        <div class="container-fluid" style="padding-right:0;">
                             <div class="bs-callout bs-callout-default">
                             <h2>' . $section->name . '</h2>
                             </div>
                             <p class="caption">' . $section->text . '</p>
                        </div>
                      </div>';
            }
            ?>
        </div>
    </div>
</div>
