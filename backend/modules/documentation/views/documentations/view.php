<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\documentation\widgets\DocumentationWidget;

/* @var $this yii\web\View */
/* @var $model backend\modules\documentation\models\Documentations */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Documentations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentations-view">


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DocumentationWidget::widget(['id' => $model->id]); ?>


</div>
