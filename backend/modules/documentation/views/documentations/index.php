<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\documentation\models\DocumentationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Documentations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentations-index">


    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Documentations', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    /* @var $model backend\modules\documentation\models\Documentations */
                    if (isset($model->status)) {
                        if ($model->status == 0)
                            return 'Private';
                        else if ($model->status == 1)
                            return 'Published';
                    } else {
                        return 'Published';
                    }

                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function ($model) {
                    /* @var $model backend\modules\documentation\models\Documentations */
                    if ($model->updated_at)
                        return Yii::$app->formatter->asDate($model->updated_at, 'dd-MM-yyyy');
                    else
                        return 'Date';
                },
                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'updated_at',
                    'language' => 'ru',
                    'options' => ['class' => 'form-control']
                ]),
                'format' => 'html',
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    /* @var $model backend\modules\documentation\models\Documentations */
                    if ($model->created_at)
                        return Yii::$app->formatter->asDate($model->created_at, 'dd-MM-yyyy');
                    else
                        return 'Date';
                },
                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'dateFormat' => 'dd-MM-yyyy',
                    'options' => ['class' => 'form-control']
                ]),
                'format' => 'html',
            ],
            ['class' => 'yii\grid\ActionColumn'],
            [
                'attribute' => '',
                'format' => 'html',
                'value' => function ($model) {
                    /* @var $model backend\modules\documentation\models\Documentations */
                    return Html::a('<i class="fa glyphicon glyphicon-download-alt"></i> PDF', \yii\helpers\Url::to(['documentations/pdf-gen', 'id' => $model->id]), [
                        'class'=>'btn btn-download-alt',
                        'target'=>'_blank',
                        'data-toggle'=>'tooltip',
                        'title'=>'Will open the generated PDF file in a new window'
                    ]);

                }
            ]
        ],
    ]); ?>
</div>

