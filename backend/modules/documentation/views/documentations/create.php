<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\documentation\models\Documentations */

$this->title = 'Create Documentations';
$this->params['breadcrumbs'][] = ['label' => 'Documentations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentations-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', compact('model', 'statusList', 'lastSectionId')) ?>
</div>
