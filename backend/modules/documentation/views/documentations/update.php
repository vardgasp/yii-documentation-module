<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\documentation\models\Documentations */

$this->title = 'Update Documentations: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Documentations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="documentations-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', compact('model', 'statusList', 'lastSectionId', 'currentSections')) ?>

</div>
