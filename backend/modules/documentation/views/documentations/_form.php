<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use backend\modules\documentation\assets\DocumentationAsset;

DocumentationAsset::register($this);


/* @var $this yii\web\View */
/* @var $model backend\modules\documentation\models\Documentations */
/* @var $form yii\widgets\ActiveForm */
/* @var $lastSectionId number */
/* @var $statusList array */
?>

<div class="documentations-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?php $model->status = 1 ?>
    <?= $form->field($model, 'status')->dropDownList($statusList) ?>
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span
                            class="glyphicon glyphicon-th-list">
                            </span> Basic Info</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $form->field($model, 'support_text')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'support_link')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'author_name')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'author_profile')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $form->field($model, 'version')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'header_bg_color')->widget(\kartik\color\ColorInput::classname(), [
                                    'options' => ['placeholder' => 'Select color ...'],
                                ]);
                                ?>
                                <?= $form->field($model, 'header_text_color')->widget(\kartik\color\ColorInput::classname(), [
                                    'options' => ['placeholder' => 'Select color ...'],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid nonpadding">
        <div class="row">
            <div class="col-md-4" id="sections">
                <input type="text" name="Documentations[sections]" onsubmit="return false" id="newSecName"
                       placeholder="Section Name"
                       class="form-control" style="margin-bottom:10px">
                <span class="addSection">Add a New Section</span>
                <div id="loadSections">
                    <ul class="sortUl">
                        <?php
                        if (isset($currentSections))
                            foreach ($currentSections as $section) {
                                echo '<li data-section-id="' . $section->id . '"><span><span style="width:100px;overflow: hidden;text-overflow: ellipsis;" class = "sectionN' . $section->id . '">' . $section->name . '</span></span><span class="rightButtons"><a class="delt"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></span><input class="hiddenSecIdd'. $section->id .'" type="hidden" name = "Documentations[sections][' . $section->id . ']" value="' . $section->name . '"></li>';
                            }
                        ?>
                    </ul>
                </div>
            </div>

            <div class="col-md-8">
                <div class="container-fluid sectionInfo">
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12" style="text-align:right;padding-top:5px;">
                            Section
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <input type="hidden" id="hdddd">
                            <input style="width:40%;background: rgba(0,0,0,0.8);border:0;color:#ffffff" type="text" class="form-control ChangeSec">
                        </div>
                    </div>

                </div>
                <div id="sectionsTextAreas">
                    <?php
                    if (isset($currentSections))
                        foreach ($currentSections as $section) {
                            echo $form->field($model, 'text[' . $section->id . ']')->textarea(['class' => 'form-control sectionTextArea', 'value' => $section->text])->label(false);
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<input type="hidden" id="hidUrl" value="<?= \yii\helpers\Url::toRoute(['default/sections']) ?>">
<input type="hidden" id="hidSectionLastId" value="<?= $lastSectionId ?>">




