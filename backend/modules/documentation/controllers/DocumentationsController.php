<?php

namespace backend\modules\documentation\controllers;

use backend\modules\documentation\models\Sections;
use backend\modules\documentation\widgets\DocumentationWidget;
use Yii;
use backend\modules\documentation\models\Documentations;
use backend\modules\documentation\models\DocumentationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;


/**
 * DocumentationsController implements the CRUD actions for Documentations model.
 */
class DocumentationsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Documentations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DocumentationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Documentation PDF.
     * @param integer $id
     * @return mixed
     */
    public function actionPdfGen($id)
    {

        $pdf = Yii::$app->pdf;
        $pdf->content = DocumentationWidget::widget(['id' => $id, 'showMenu' => false]);;
        return $pdf->render();


    }

    /**
     * Displays a single Documentations model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Documentations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Documentations();
        $statusList = array('0' => 'Private', '1' => 'Published', '2' => 'Draft', '3' => 'Trash');
        $lastSectionId = Sections::find()->orderBy('id DESC')->all() ? Sections::find()->orderBy('id DESC')->limit(1)->one() : 0;
        if($lastSectionId != null) {
            $lastSectionId = $lastSectionId->id;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', compact('model', 'statusList', 'lastSectionId'));
        }
    }

    public function actions()
    {

        return [
            'browse-images' => [
                'class' => 'bajadev\ckeditor\actions\BrowseAction',
                'url' => '/images/uploads/',
                'path' => '@frontend/web/images/uploads/',
            ],
            'upload-images' => [
                'class' => 'bajadev\ckeditor\actions\UploadAction',
                'url' => '/images/uploads/',
                'path' => '@frontend/web/images/uploads/',
            ],
        ];
    }


    /**
     * Updates an existing Documentations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $statusList = array('0' => 'Private', '1' => 'Published', '2' => 'Draft', '3' => 'Trash');
        $lastSectionId = Sections::find()->orderBy('id DESC')->all() ? Sections::find()->orderBy('id DESC')->limit(1)->one() : 0;
        if($lastSectionId != null) {
            $lastSectionId = $lastSectionId->id;
        }
        $currentSections = Sections::find()->where(['documentation_id' => $model->id])->orderBy('sort_id ASC')->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', compact('model', 'statusList', 'lastSectionId', 'currentSections'));
        }
    }

    /**
     * Deletes an existing Documentations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Documentations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Documentations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Documentations::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
