$(document).ready(function () {
    $('body').on('keypress', '#newSecName', function(event) {
        if(event.which == 13) {
            event.preventDefault();
            $('.addSection').trigger('click');
        }
    });

    $('ul.sortUl').sortable();
    var hidUrl = $('#hidUrl').val();
    $('.sectionTextArea').hide();

    newSectionId = $('#hidSectionLastId').val();

    $('ul.sortUl li').eq(0).addClass('active');
    $('.addSection').click(function () {
        var newSectionName = $('#newSecName').val();
        if(newSectionName != '') {
            $('ul.sortUl').prepend('<li data-section-id="' + ++newSectionId + '"><span>Section Name : <span class = "msection sectionN' + newSectionId + '">' + newSectionName + '</span></span><span class="rightButtons"><a class="delt"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></span>' + '<input class="hiddenSecIdd'+newSectionId+'" type="hidden" name = "Documentations[sections][' + newSectionId + ']" value="' + newSectionName +'">' + '</li>');
            var appendSection = '<div class="form-group field-documentations-text-'+ newSectionId +'"><textarea id="documentations-text-' + newSectionId + '" class="form-control sectionTextArea" name="Documentations[text][' + newSectionId + ']"></textarea><div class="help-block"></div></div>';
            $('#sectionsTextAreas').append(appendSection);
            $('input.ChangeSec').val(newSectionName).attr('readOnly', 'readonly');
            $('ul.sortUl li').removeClass('active');
            $('ul.sortUl li').eq(0).addClass('active');
            $('.sectionTextArea').hide();
            $('.sectionTextArea').parent().hide();
            $('#documentations-text-' + newSectionId).parent().show();
            try {
                CKEDITOR.replace("documentations-text-" + newSectionId, {"preset":"full","filebrowserBrowseUrl":"browse-images","filebrowserUploadUrl":"upload-images","extraPlugins":"imageuploader","on":{"instanceReady":function( ev ){bajadev.ckEditor.registerOnChange("page-content"); bajadev.ckEditor.registerCsrf();}}});
            } catch(e) {
            }
        }

        $('.field-documentations-text-1 label').html('Text for Section : ' + newSectionId);
        $('#newSecName').val('');
        rightClick();
    });
    rightClick();

    function rightClick() {
        $('.rightButtons a.delt').unbind('click').bind('click', function (e)  {
            e.preventDefault();
            id = $(this).parent().parent().data('sectionId');
            result = confirm('Are you sure ? ');
            if(result) {
                $('div.field-documentations-text-' + id).remove();
                $(this).parent().parent().remove();
                var LastSectionId = $('#hidSectionLastId').val();
            }
        });
        $('ul.sortUl li').click(function () {
            id = $(this).data('sectionId');
            var secName = $('.sectionN' + id).text();
            $('input.ChangeSec').val(secName);
            $('input.ChangeSec').removeAttr('readOnly');
            $('ul.sortUl li').removeClass('active');
            $('ul.sortUl li[data-section-id="' + id + '"]').addClass('active');
            $('.sectionTextArea').parent().hide();
            $('#documentations-text-' + id).parent().show();
            try {
                CKEDITOR.replace("documentations-text-" + id, {"preset":"full","filebrowserBrowseUrl":"browse-images","filebrowserUploadUrl":"upload-images","extraPlugins":"imageuploader","on":{"instanceReady":function( ev ){bajadev.ckEditor.registerOnChange("page-content"); bajadev.ckEditor.registerCsrf();}}});
            } catch(e){}
            $('input.ChangeSec').keyup(function() {
                $('span.sectionN' + id).text($('input.ChangeSec').val());
                $('input.hiddenSecIdd' + id).val($('span.sectionN' + id).text());
            });
        });
    }

});