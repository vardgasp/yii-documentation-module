$(document).ready(function () {

    if ($(window).width() >= 1200) {
        try {
            if ($(window).scrollTop() < $('.bs-callout').eq(0).offset().top - 100) {
                $('.nav-stacked').css({
                    'width:': $('.col-md-3').outerWidth(),
                    'position': 'fixed',
                    'top': $('.nav-stacked').offset().top
                });
            } else {
                $('.nav-stacked').css({'width:': $('.col-md-3').outerWidth(), 'position': 'fixed', 'top': '70px'});
            }
            var x = 0;
            $(document).scroll(function () {
                if ($(window).scrollTop() >= $('.bs-callout').eq(0).offset().top - 100) {
                    if (x == 0) {
                        $('.nav-stacked').css({
                            'width:': $('.col-md-3').outerWidth(),
                            'position': 'fixed',
                            'top': $('.bs-callout').eq(0).offset().top
                        });
                        $('.nav-stacked').animate({'top': '70px'}, 500);
                    }
                    x = 1;
                } else {
                    if (x == 1) {
                        $('.nav-stacked').css({
                            'width:': $('.col-md-3').outerWidth(),
                            'position': 'fixed',
                            'top': '70px',
                            'opacity': 0
                        });
                        $('.nav-stacked').animate({'top': $('.bs-callout').eq(0).offset().top, 'opacity': '1'}, 1);
                    }
                    x = 0;
                }
            });
        } catch (e) {
        }

    }

    // navigation click actions
    $('.scroll-link').on('click', function (event) {
        event.preventDefault();
        $('.scroll-link').removeClass('active');
        $(this).addClass('active');
        var sectionID = $(this).attr("data-id");
        scrollToID('#' + sectionID, 750);
    });
    // scroll to top action
    $('.scroll-top').on('click', function (event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 'slow');
    });
    // mobile nav toggle
    $('#nav-toggle').on('click', function (event) {
        event.preventDefault();
        $('#main-nav').toggleClass("open");
    });
});
// scroll function
function scrollToID(id, speed) {
    var offSet = 50;
    var targetOffset = $(id).offset().top - offSet;
    var mainNav = $('#main-nav');
    $('html,body').animate({scrollTop: targetOffset}, speed);
    if (mainNav.hasClass("open")) {
        mainNav.css("height", "1px").removeClass("in").addClass("collapse");
        mainNav.removeClass("open");
    }
}
if (typeof console === "undefined") {
    console = {
        log: function () {
        }
    };
}