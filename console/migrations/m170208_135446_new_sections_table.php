<?php

use yii\db\Migration;

class m170208_135446_new_sections_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%sections}}', [
            'id' => $this->primaryKey(),
            'documentation_id' => $this->integer(),
            'name' => $this->string(),
            'text' => $this->text(),
            'sort_id' => $this->integer()
        ]);
        $this->addForeignKey(
            'sections_documentation_id',
            '{{%sections}}',
            'documentation_id',
            '{{%documentations}}',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%sections}}');

        $this->dropForeignKey('sections_documentation_id', '{{%sections}}');
    }
}
