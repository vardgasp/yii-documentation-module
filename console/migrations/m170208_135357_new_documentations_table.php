<?php

use yii\db\Migration;

class m170208_135357_new_documentations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%documentations}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'status' => $this->string(),
            'updated_at' => $this->string(),
            'created_at' => $this->string(),
            'version' => $this->string(),
            'support_text' => $this->string(),
            'support_link' => $this->string(),
            'author_name' => $this->string(),
            'author_profile' => $this->string(),
            'header_bg_color' => $this->string(),
            'header_text_color' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%documentations}}');
    }
}
